﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using ExamenLib1;

namespace Examen1App
{
    internal class Program
    {
        public static List<PlayList> ListeChoix = new List<PlayList>();
        public static bool IsProgramRunning = true;
        public static int userChoice = 0;
        public static string resetOn = "";
        public static void Main(string[] args)
        {
            while (IsProgramRunning)
            {
                Console.Clear();
                if (ListeChoix.Count == 0) GeneratePlaylists();
                PrintListOfChoices(ListeChoix);
                do
                {
                    Console.Write("Entrez votre choix de playlist : ");
                    int.TryParse(Console.ReadLine(), out userChoice);
                } while (userChoice < 1 || userChoice > ListeChoix.Count);
                Console.WriteLine("Vous avez choisi : " + ListeChoix[userChoice - 1].Name + "\n" + ListeChoix[userChoice - 1]);
                Console.WriteLine("La liste à une durée de " + Song.FormatTime(ListeChoix[userChoice-1].TotalLength()));
                do
                {
                    Console.Write("Choisir une autre liste O/N ? ");
                    resetOn = Console.ReadLine().ToUpper();
                } while (resetOn != "N" && resetOn != "O");
                if (resetOn == "N")
                {
                    IsProgramRunning = false;
                    Console.WriteLine("Merci et bonne journée");
                }
            }
        }
        private static void PrintListOfChoices(List<PlayList> listeChoix)
        {
            for (int i = 0; i < listeChoix.Count; i++)
            {
                Console.WriteLine((i + 1) + " : " + listeChoix[i].Name);
            }
        }
        private static void GeneratePlaylists()
        {
            PlayList tinaTournee = new PlayList("Tina Tournée");
            tinaTournee.AddSong(new Song("What's love", "Tina Tournée", 121));
            tinaTournee.AddSong(new Song("Z'got to do", "Tina Tournée", 144));
            tinaTournee.AddSong(new Song("et With it", "Tina Tournée", 155));
            tinaTournee.AddSong(new Song("Peel off that man", "Tina Tournée", 210 ));
            ListeChoix.Add(tinaTournee);
            PlayList tataBoutlamine = new PlayList("Tata Boutlamine");
            tataBoutlamine.AddSong(new Song("Do dièse", "Tata Boutlamine",212));
            tataBoutlamine.AddSong(new Song("Narine", "Tata Boutlamine",280));
            tataBoutlamine.AddSong(new Song("Sol bémol", "Tata Boutlamine",212));
            tataBoutlamine.AddSong(new Song("Mi majeur", "Tata Boutlamine",92));
            ListeChoix.Add(tataBoutlamine);
            PlayList celestinLaperriere = new PlayList("Celestin Laperrière");
            celestinLaperriere.AddSong(new Song("Je pleure sur tes valises", "Celestin Laperrière", 260));
            celestinLaperriere.AddSong(new Song("Toi et moi dans l'couloir", "Celestin Laperrière", 182));
            celestinLaperriere.AddSong(new Song("Pas vraiment interessé", "Celestin Laperrière", 69));
            celestinLaperriere.AddSong(new Song("Derrière cette grange", "Celestin Laperrière", 320));
            ListeChoix.Add(celestinLaperriere);
        }
    }
}