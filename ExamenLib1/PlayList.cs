﻿using System;
using System.Collections.Generic;

namespace ExamenLib1
{
    public class PlayList
    {
        public string Name { get; set; }
        public List<Song> SongList { get; set; }
        public int Count { get => SongList.Count;}

        public PlayList(string name)
        {
            if (name.Length < 1)
            {
                throw new Exception("Le nom de la playlist doit contenir au moins un caractère");
            }
            else
            {
                Name = name;    
            }
            SongList = new List<Song>();
        }

        public void AddSong(Song song)
        {
            SongList.Add(song);
        }

        public override string ToString()
        {
            Console.Clear();
            string compositeString = $"La playlist '" + Name + "' a " + Count + " chansons\n";
            foreach (Song song in SongList)
            {
                compositeString += song + "\n";
            }
            return compositeString;
        }

        public int TotalLength()
        {
            int totalSeconds = 0;
            foreach (Song song in SongList)
            {
                totalSeconds += song.Seconds;
            }
            return totalSeconds;
        }
    }
}