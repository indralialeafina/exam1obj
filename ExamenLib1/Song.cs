﻿using System;

namespace ExamenLib1
{
    public class Song
    {
        public static int NextId { get; private set; } = 1;
        public int Id { get; set; }
        public string Title { get; set; }
        public string Artist { get; set; }
        public int Seconds { get; set; }
        public double Minutes { get; set;}

        public Song(string title, string artist, int seconds)
        {
            Id = NextId;
            NextId++;
            if (title.Length < 1)
            {
                throw new Exception("Le titre doit avoir plus d'un caractère");
            }
            else
            {
                Title = title;
            }

            if (artist.Length < 3)
            {
                throw new Exception("Le nom de l'artiste doit contenir au moins 3 caractères");
            }
            else
            {
                Artist = artist;    
            }

            if (seconds < 1)
            {
                throw new Exception("La durée de la chanson doit être d'au moins une seconde !!");
            }
            else
            {
                Seconds = seconds;    
            }
            Minutes = (double) seconds / 60;
        }
        public override string ToString()
        {
            return $"{Id,-2}: {Title} ({Artist}) {FormatTime(Seconds)}";
        }
        public static string FormatTime(int seconds)
        {
            int runTimeMinutes = seconds / 60;
            int runTimeSeconds = seconds % 60;
            string padding0 = runTimeSeconds < 10 ? "0" : "";
            return $"{runTimeMinutes}:{padding0}{runTimeSeconds}";
        }
        public override bool Equals(object obj)
        {
            if (obj == null || this.GetType() != obj.GetType())
            {
                return false;
            }
            Song other = (Song)obj;
            return Id == other.Id;
        }
        public override int GetHashCode()
        {
            return Id;
        }
    }
}